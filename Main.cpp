
// Playing Cards

#include <iostream>
#include <conio.h>



using namespace std;

enum Rank
{
    Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace
};

enum Suit
{
    Clubs, Diamonds, Hearts, Spades
};

struct Card
{
    Rank rank;
    Suit suit;
};

void PrintCard(Card card);



void PrintCard(Card card)
{
    switch (card.rank)
    {
    case Two:;
        cout << "The Two of ";
        break;
    case Three:;
        cout << "The Three of ";
        break;
    case Four:;
        cout << "The Four of ";
        break;
    case Five:;
        cout << "The Five of ";
        break;
    case Six:;
        cout << "The Six of ";
        break;
    case Seven:;
        cout << "The Seven of ";
        break;
    case Eight:;
        cout << "The Eight of ";
        break;
    case Nine:;
        cout << "The Nine of ";
        break;
    case Ten:;
        cout << "The Ten of ";
        break;
    case Jack:;
        cout << "The Jack of ";
        break;
    case Queen:;
        cout << "The Queen of ";
        break;
    case King:;
        cout << "The King of ";
        break;
    case Ace:;
        cout << "The Ace of ";
        break;
    default:
        cout << "Not a card rank";
    }
    switch (card.suit)
    {
    case Hearts: cout << "Hearts.\n";
        break;
    case Diamonds: cout << "Diamonds.\n";
        break;
    case Clubs: cout << "Clubs.\n";
        break;
    case Spades: cout << "Spades.\n";
        break;
    default:
        cout << "Not a card rank";
    }
}





Card HighCard(Card card1, Card card2)
{
    int rank1;
    int rank2;
    switch (card1.rank)
    {
    case Two: rank1 = 2;
        cout << "The Two of ";
        break;
    case Three: rank1 = 3;
        cout << "The Three of ";
        break;
    case Four: rank1 = 4;
        cout << "The Four of ";
        break;
    case Five: rank1 = 5;
        cout << "The Five of ";
        break;
    case Six: rank1 = 6;
        cout << "The Six of ";
        break;
    case Seven: rank1 = 7;
        cout << "The Seven of ";
        break;
    case Eight: rank1 = 8;
        cout << "The Eight of ";
        break;
    case Nine: rank1 = 9;
        cout << "The Nine of ";
        break;
    case Ten: rank1 = 10;
        cout << "The Ten of ";
        break;
    case Jack: rank1 = 11;
        cout << "The Jack of ";
        break;
    case Queen: rank1 = 12;
        cout << "The Queen of ";
        break;
    case King: rank1 = 13;
        cout << "The King of ";
        break;
    case Ace: rank1 = 14;
        cout << "The Ace of ";
        break;
    default:
        cout << "Not a card rank";
    }
    switch (card1.suit)
    {
    case Hearts: cout << "Hearts.\n";
        break;
    case Diamonds: cout << "Diamonds.\n";
        break;
    case Clubs: cout << "Clubs.\n";
        break;
    case Spades: cout << "Spades.\n";
        break;
    default:
        cout << "Not a card rank";
    }


    switch (card2.rank)
    {
    case Two: rank2 = 2;
        cout << "The Two of ";
        break;
    case Three: rank2 = 3;
        cout << "The Three of ";
        break;
    case Four: rank2 = 4;
        cout << "The Four of ";
        break;
    case Five: rank2 = 5;
        cout << "The Five of ";
        break;
    case Six: rank2 = 6;
        cout << "The Six of ";
        break;
    case Seven: rank2 = 7;
        cout << "The Seven of ";
        break;
    case Eight: rank2 = 8;
        cout << "The Eight of ";
        break;
    case Nine: rank2 = 9;
        cout << "The Nine of ";
        break;
    case Ten: rank2 = 10;
        cout << "The Ten of ";
        break;
    case Jack: rank2 = 11;
        cout << "The Jack of ";
        break;
    case Queen: rank2 = 12;
        cout << "The Queen of ";
        break;
    case King: rank2 = 13;
        cout << "The King of ";
        break;
    case Ace: rank2 = 14;
        cout << "The Ace of ";
        break;
    default:
        cout << "Not a card rank";
    }
    switch (card2.suit)
    {
    case Hearts: cout << "Hearts.\n";
        break;
    case Diamonds: cout << "Diamonds.\n";
        break;
    case Clubs: cout << "Clubs.\n";
        break;
    case Spades: cout << "Spades.\n";
        break;
    default:
        cout << "Not a card rank";
    }

    if (rank1 < rank2)
    {
        cout << "The second card has the higher rank.";
    }
    if (rank2 < rank1)
    {
        cout << "The first card has the higher rank.";
    }
    else
    {
        cout << "These cards' ranks are equal.";
    }

    return card1, card2;
}

int main()
{


    Card card;
    card.rank = Ace;
    card.suit = Spades;

    PrintCard(card);
    cout << endl;

    

    Card card1;
    card1.rank = Nine;
    card1.suit = Hearts;
    Card card2;
    card2.rank = Seven;
    card2.suit = Clubs;

    HighCard(card1, card2);

    _getch();
    return 0;
}